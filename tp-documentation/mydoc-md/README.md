#This is mydoc

##Licenses

This project... [license.txt]
(license.txt)

##List

-item 1
-item 2

##Table

|column1|column2|
|-------|-------|
|foo    |bar    |
|toto   |tata   |

##Code

```haskell
main :: IO ()
main = putStrLn "hello"

##Quote

> message
>
> (moi)

## Image

 ![]https://upload.wikipedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/langfr-105px-Visual_Studio_Code_1.35_icon.svg.png